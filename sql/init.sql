-- user manage app set up
CREATE TABLE customers (
    customer_id SERIAL PRIMARY KEY,
    first_name VARCHAR(30), 
    last_name VARCHAR(30),
    gender CHAR(1),
    date_of_birth DATE,
    email VARCHAR(50) UNIQUE,
    mobile VARCHAR(20) UNIQUE
);

CREATE TABLE income (
    income_id SERIAL PRIMARY KEY,
    income_details VARCHAR(50),
    income_amount NUMERIC(8,2),
    income_date DATE,
    customer_id INT REFERENCES customers (customer_id)
);  